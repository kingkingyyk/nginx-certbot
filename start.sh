nginx &&
sleep 5 &&
while true
do
  echo "Running certbot"
  certbot renew --quiet --nginx
  sleep 86400 # Wait for 1 day.
done
