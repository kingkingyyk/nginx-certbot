FROM nginx:1.23.1

ENV PYTHONUNBUFFERED=1

RUN apt update -y &&\
    apt install python3 python3-pip python3-dev libssl-dev musl-dev libffi-dev -y &&\
    apt clean &&\
    pip3 install -U pip &&\
    pip3 install --no-cache-dir -U certbot-nginx &&\
    mkdir /etc/letsencrypt &&\
    rm /etc/nginx/conf.d/default.conf

ADD start.sh /

CMD ["sh", "start.sh"]
