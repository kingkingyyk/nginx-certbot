# Nginx-CertBot

This is an image based on `nginx` official Docker image with `python-certbot-nginx` to provide both hosting & automatic certificate renewal everyday.

Let's Encrypt's data is stored in `/etc/letsencrypt`, please mount a folder here for persistent storage.
